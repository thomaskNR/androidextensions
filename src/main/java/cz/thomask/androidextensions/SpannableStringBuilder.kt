package cz.thomask.androidextensions

import android.text.SpannableStringBuilder
import android.text.Spanned

fun SpannableStringBuilder.appendTextWithSpans(text: CharSequence?, vararg span: Any?): SpannableStringBuilder {
    if (text != null) {
        val start: Int = length
        val size = text.length
        append(text)
        for (o in span) {
            setSpan(o, start, start + size, Spanned.SPAN_INCLUSIVE_EXCLUSIVE)
        }
    }
    return this
}
