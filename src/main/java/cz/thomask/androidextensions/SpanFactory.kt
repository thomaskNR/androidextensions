package cz.thomask.androidextensions

import android.graphics.Typeface
import android.text.Layout
import android.text.style.*

class SpanFactory() {

    fun getRelativeSizeSpan(ratio: Float): Any {
        return RelativeSizeSpan(ratio)
    }

    val fontBoldSpan: Any
        get() = StyleSpan(Typeface.BOLD)

    val fontItalicSpan: Any
        get() = StyleSpan(Typeface.ITALIC)

    val fontRegularSpan: Any
        get() = StyleSpan(Typeface.NORMAL)

    fun getAbsoluteSizeSpan(height: Int, dp: Boolean): Any {
        return AbsoluteSizeSpan(height, dp)
    }

    val centerAlignSpan: Any
        get() = AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER)

    val normalAlignSpan: Any
        get() = AlignmentSpan.Standard(Layout.Alignment.ALIGN_NORMAL)

    val oppositeAlignSpan: Any
        get() = AlignmentSpan.Standard(Layout.Alignment.ALIGN_OPPOSITE)

    fun getForegroundColorSpan(color: Int): Any {
        return ForegroundColorSpan(color)
    }

    val upperIndexSpan: Any
        get() = SuperscriptSpan()

    val downIndexSpan: Any
        get() = SubscriptSpan()


}
