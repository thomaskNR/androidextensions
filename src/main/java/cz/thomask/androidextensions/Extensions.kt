package cz.thomask.androidextensions

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.res.Configuration
import android.net.Uri
import android.provider.Settings
import android.util.Base64
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import java.io.InputStream
import java.text.Normalizer
import kotlin.experimental.and

inline fun Context.intent(action: String, body: Intent.() -> Unit): Intent {
    val intent = Intent(action)
    intent.body()
    return intent
}

inline fun Context.intent(actionClass: Class<*>, body: Intent.() -> Unit): Intent {
    val intent = Intent(this, actionClass)
    intent.body()
    return intent
}

fun Context.isLandscape(): Boolean {
    return this.resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE
}

fun Context.convertDpToPx(dp: Float): Float {
    return dp * this.getResources().getDisplayMetrics().density
}

fun Context.convertPxToDp(px: Float): Float {
    return px / this.getResources().getDisplayMetrics().density
}

fun Context.getRawFileInputStream(ID: Int): InputStream? {
    return this.getResources().openRawResource(ID)
}

fun View.hide() {
    this.visibility = View.GONE
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

inline var View.isVisible: Boolean
    get() = visibility == View.VISIBLE
    set(value) {
        visibility = if (value) View.VISIBLE else View.GONE
    }

inline var View.isHidden: Boolean
    get() = visibility == View.GONE
    set(value) {
        visibility = if (value) View.GONE else View.VISIBLE
    }

inline var View.isInvisible: Boolean
    get() = visibility == View.INVISIBLE
    set(value) {
        visibility = if (value) View.INVISIBLE else View.VISIBLE
    }


inline fun <T : Any> LiveData<T>.observeWith(lifecycleOwner: LifecycleOwner, crossinline onChanged: (T) -> Unit) {
    observe(lifecycleOwner, Observer {
        it ?: return@Observer
        onChanged.invoke(it)
    })
}

fun Context.createLauncherIntentForActivity(launchActivityFromManifest: Class<*>): Intent {
    val notificationIntent = Intent(this, launchActivityFromManifest)
    notificationIntent.action = Intent.ACTION_MAIN
    notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER)
    return notificationIntent
}

fun Collection<String>.makeIntentFilter(): IntentFilter {
    val filter = IntentFilter()
    for (s in this) {
        filter.addAction(s)
    }
    return filter
}

fun String.makeIntentFilter(): IntentFilter {
    return IntentFilter().also { it.addAction(this) }
}

fun Context.viewUrl(url: String, newTask: Boolean = false): Boolean {
    try {
        val intent = intent(Intent.ACTION_VIEW) {
            data = Uri.parse(url)
            if (newTask) addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        startActivity(intent)
        return true
    } catch (e: Exception) {
        return false
    }
}

fun Context.openSettingsOfApp(packagename: String) {
    val uri = Uri.fromParts("package", packagename, null)
    val intent = Intent()
    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
    intent.data = uri
    this.startActivity(intent)
}

fun Activity.shareText(text: String) {
    val sendIntent = Intent()
    sendIntent.action = Intent.ACTION_SEND
    sendIntent.putExtra(Intent.EXTRA_TEXT, text)
    sendIntent.type = "text/plain"
    startActivity(sendIntent)
}


fun String.removeDiacritics(): String {
    var text = this
    text = Normalizer.normalize(text, Normalizer.Form.NFD)
    text = text.replace("[^\\p{ASCII}]".toRegex(), "")
    return text
}

fun Context.share(text: String, subject: String = ""): Boolean {
    val intent = Intent()
    intent.type = "text/plain"
    intent.putExtra(Intent.EXTRA_SUBJECT, subject)
    intent.putExtra(Intent.EXTRA_TEXT, text)
    try {
        startActivity(Intent.createChooser(intent, null))
        return true
    } catch (e: ActivityNotFoundException) {
        return false
    }
}

val hexArray = "0123456789ABCDEF".toCharArray()
fun ByteArray.bytesToHex(): String {
    val hexChars = CharArray(this.size * 2)
    for (j in this.indices) {
        val v: Int = (this[j] and 0xFF.toByte()).toInt()
        hexChars[j * 2] = hexArray.get(v ushr 4)
        hexChars[j * 2 + 1] = hexArray.get(v and 0x0F)
    }
    return String(hexChars)
}

fun ByteArray.bytesToHexBytesSeparated(): String {
    val hexChars = CharArray(this.size * 3)
    for (j in this.indices) {
        val v: Int = (this[j] and 0xFF.toByte()).toInt()
        hexChars[j * 3] = hexArray.get(v ushr 4)
        hexChars[j * 3 + 1] = hexArray.get(v and 0x0F)
        hexChars[j * 3 + 2] = ' '
    }
    return String(hexChars)
}

fun String.hexToByteArray(): ByteArray? {
    val input = this.replace(" ", "")
    val len = input.length
    try {
        val data = ByteArray(len / 2)
        var i = 0
        while (i < len) {
            data[i / 2] = ((Character.digit(input[i], 16) shl 4) + Character.digit(input[i + 1], 16)).toByte()
            i += 2
        }
        return data
    } catch (ex: Exception) {
        return null
    }
}

fun String.convertToBase64String(): String {
    return this.toByteArray(charset("UTF-8")).convertToBase64String()
}

fun ByteArray.convertToBase64String(): String {
    return Base64.encodeToString(this, Base64.DEFAULT)
}

fun String.convertFromBase64ToByte(): ByteArray {
    return Base64.decode(this.toByteArray(charset("UTF-8")), Base64.DEFAULT)
}

fun String.convertToBase64Byte(): ByteArray {
    return Base64.encode(this.toByteArray(charset("UTF-8")), Base64.DEFAULT)
}

fun View.hideKeyboard() {
    val imm = this.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(this.windowToken, 0)
}

