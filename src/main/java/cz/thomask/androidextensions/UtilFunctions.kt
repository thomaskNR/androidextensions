package cz.thomask.androidextensions

import android.content.IntentFilter
import android.view.View
import java.io.IOException
import java.net.URL

object CommonFunctions {
    fun makeIntentFilter(vararg actions: String?): IntentFilter {
        val filter = IntentFilter()
        actions.filterNotNull().forEach { filter.addAction(it) }
        return filter
    }

    fun isURLAvailable(urlToCheckAvailability: String?): Boolean {
        var logserverAvailable = false
        try {
            val url = URL(urlToCheckAvailability).openConnection().apply {
                setRequestProperty("User-Agent", "Android Application")
                setRequestProperty("Connection", "close")
                connectTimeout = 10 * 1000
                connect()
            }
            logserverAvailable = true
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return logserverAvailable
    }

    fun logBooleanValues(vararg valuesToLog: Boolean): String {
        return valuesToLog.joinToString(prefix = "[", postfix = "]")
    }

    fun getHeapSize(): Long {
        return Runtime.getRuntime().totalMemory()
    }

    fun getAllocatedSize(): Long {
        return Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()
    }

    fun hideViews(vararg views: View) {
        views.forEach { it.visibility = View.GONE }
    }

    fun showViews(vararg views: View) {
        views.forEach { it.visibility = View.VISIBLE }
    }

    fun invisibleViews(vararg views: View) {
        views.forEach { it.visibility = View.INVISIBLE }
    }
}


